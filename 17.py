PS: Use the functions to find biggest and smallest numbers.'''
#17
#with If.... else condition
print("with If.... else condition")
num1 = int(input('Enter First number : '))
num2 = int(input('Enter Second number : '))
num3 = int(input('Enter Third number : '))
def largest(num1, num2, num3):
    if (num1 > num2) & (num1 > num3):
        largest_num = num1
    elif (num2 > num1) & (num2 > num3):
        largest_num = num2
    else:
        largest_num = num3
    print("The largest of the 3 numbers is : ", largest_num)
def smallest(num1, num2, num3):
    if (num1 < num2) & (num1 < num3):
        smallest_num = num1
    elif (num2 < num1) & (num2 < num3):
        smallest_num = num2
    else:
        smallest_num = num3
    print("The smallest of the 3 numbers is : ", smallest_num)
largest(num1, num2, num3)
smallest(num1, num2, num3)

#method 2


Num = []
Number = int(input("Please enter the Total Number of List Elements: "))
for i in range(1, Number + 1):
    value = int(input("Please enter the Value of %d Element : " %i))
    Num.append(value)
print("The Smallest Element in this List is : ", min(Num))
print("The Largest Element in this List is : ", max(Num))