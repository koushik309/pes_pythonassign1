#Command line Arguments
'''5.Write a program to receive 5 command line arguments and print each argument separately. Example : >> python test.py arg1 arg2 arg3 arg4 arg5 a) From the above statement your program should receive arguments and print them each of them. b) Find the biggest of three numbers, where three numbers are passed as command line arguments.'''
import sys
a1=sys.argv[1]
a2=sys.argv[2]
a3=sys.argv[3]
a4=sys.argv[4]
a5=sys.argv[5]

print ("arg1= ",a1)
print ("arg2= ",a2)
print ("arg3= ",a3)
print ("arg4= ",a4)
print ("arg5= ",a5)

if a3>a4 and a3>a5:
  print ("the biggest no is",a3)
elif a4>a3 and a4>a5:
  print ("the biggest no is",a4)
else:
  print ("the biggest no is",a5)
