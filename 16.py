#Python Numbers
'''16.i) Check whether given number is prime or not.
ii) Generate all the prime numbers between 1 to N where N is given number.'''

pri=int(input("Enter the number to check for prime: "))
if pri==1:
  print ("Number",pri,"is not a prime number")
elif pri>1:
  for i in range(2,pri):
    if pri%i==0:
      print ("Number",pri," is not a prime number")
      break
  else:
    print ("Number",pri,"is a prime number")
nu=int(input("Enter the number till which you need to print the prime numbers: "))
print ("The prime numbers between the given range is: ")
for pri in range(2,nu+1):
  if pri>1:
    for i in range(2,pri):
      if(pri%i)==0:
        break
    else:
      print (pri)