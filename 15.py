#Python List functions and Methods
'''15.Create a list of 5 names and check given name exist in the List.
a) Use membership operator ( IN ) to check the presence of an element. 
b) Perform above task without using membership operator. 
c) Print the elements of the list in reverse direction.'''

#a) 
list=['Bohr','Albert','Einstein','Schrodinger','Newton']
print("method1")
if ('Newton') in list:
  print("Ravi is present in the list")
else:
  print("Ravi is not present in the list")

#b) 
print("method2")
for i in list:
  if (i=='Newton'):
    print("Newton is present in the list")
    break
else:
    print("Newton is not present in the list")

#c)
print("method3")
list1.reverse()
print("the elements of list1 in reverse direction are",list)