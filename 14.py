#Python List functions and Methods
'''14.Write a program to create two list A and B such that List A contains Employee Id, List B contains Employee name ( minimum 10 entries in each list ) and perform following operations 
a) Print all names on to screen 
b) Read the index from the user and print the corresponding name from both list. 
c) Print the names from 4th position to 9th position 
d) Print all names from 3rd position till end of the list 
e) Repeat list elements by specified number of times ( N- times, where N is entered by user) 
f) Concatenate two lists and print the output. 
g) Print element of list A and B side by side.( i.e. List-A First element , List-B First element )'''

listEmpId=[10,20,30,40,50,60,70,80,90,100]
listEmpName=['Ram','Sridhar','Ashish','priya','Shivani','Koushik','Shiva','Hari','Shri','William']
print (listEmpName)
index=int(input("Enter the index to read from: "))
print (listEmpName[index])
print (listEmpId[index])
print (listEmpName[3:-1])
print (listEmpName[2:])
ntime=int(input("enter the no of times you wish to repeat the list: "))
print (listEmpId*ntime)
print (listEmpName*ntime)
concatList=listEmpId+listEmpName
print (concatList)
for element in range(len(listEmpId)):
  print ("ListEmpid element",listEmpId[element],"listEmpName elements",listEmpName[element])